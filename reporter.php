<?php

/**
*
* Caorda Site Reporter
*
* Generates a report of available sites and
* a readout of CMS type, version, and URL.
*
* Author: Eric McNiece
* Date: September 10 2014
* Version: 1.2
*
* Usage:
*
* http://reporter.develapache							Human-readable with index.php
* http://reporter.develapache/reporter.php 				Standalone
* 	?fmt=rss											RSS feed
* 	?fmt=json											JSON output
* 	?nodisk=true										Doesn't add disk size info
* 	?output=filename&fmt=[xml|json] 					Dumps to file. Must specify format
**/

if ( !class_exists( 'CSReporter' ) ){
class CSReporter{

	/*=================================
	=            Init Vars            =
	=================================*/

	// Update creds if needed
	public $db_cred = array(
		'DB_NAME' => 'dbispconfig',
		'DB_USER' => 'caorda.qa',
		'DB_PASS' => 'Sidney.11022',
		'DB_HOST' => 'mysqlqa.caorda.local',
		'DB_TBL' => 'web_domain',
	);
	public $db_error = "";
	public $sites = array();
	public $json = "";
	public $xml = "";
	public $stats = array();


	/*=====================================
	=            Init Class               =
	=====================================*/

	public function __construct($db_cred=false){

		date_default_timezone_set('America/Vancouver');
		$this->stats['time_start'] = microtime(true);

		// Init db credentials
		if(!$db_cred) $db_cred = $this->db_cred;

		$db = mysql_connect($db_cred['DB_HOST'], $db_cred['DB_USER'], $db_cred['DB_PASS'] );
		mysql_select_db($db_cred['DB_NAME']);

		if(mysql_error()) $this->db_error = mysql_error();
		else $this->run($db_cred);

	} // init_db

	/*================================
	=            Init App            =
	================================*/
	public function run($db_cred=false){

		// Grab sites from db
		$msc1 = microtime(true);
		$this->get_sites($db_cred);
		$this->stats['get_sites'] = microtime(true)-$msc1;


		// Load each site with CMS info
		$msc2 = microtime(true);
		$this->get_cms_info($this->sites);
		$this->stats['time_get_cms_info'] = microtime(true)-$msc2;

		// Grab Google Analytics from CRON run
		if(!isset($_GET['noga'])){
			$mcs4 = microtime(true);
			$this->get_ga_code($this->sites);
			$this->stats['time_get_ga_data'] = microtime(true)-$mcs4;
		}

		// Get disk size for each site
		if(!isset($_GET['nodisk'])){
			$msc3 = microtime(true);
			$this->get_disk_use($this->sites);
			$this->stats['time_get_disk_use'] = microtime(true)-$msc3;
		}

		// Summarize times
		$this->stats['time_total'] = round(microtime(true) - $this->stats['time_start'], 3);
		$this->stats['time_request'] = date('c', $_SERVER['REQUEST_TIME']);
		$this->stats['memory_usage'] = round(memory_get_usage(true)/1048576,2);

		// Format into appropriate structure
		$xml = $this->format_xml($this->sites, $this->stats);
		$json = $this->format_json($this->sites);


	}

	/*====================================
	=            Query Tables            =
	====================================*/

	public function get_sites($params=false){

		$sites = $site = array();

		// This query currently selects parent domains instead of multiple child bindings.
		$query = sprintf("SELECT d.domain_id, d.domain, d.document_root, d.active, d.parent_domain_id,
						(SELECT GROUP_CONCAT(b.domain ORDER BY b.domain SEPARATOR '|')
						FROM %s.%s b
						WHERE (b.domain_id = d.domain_id) OR (b.parent_domain_id = d.domain_id)) AS bindings
					FROM %s.%s d
					ORDER BY d.domain_id",
					mysql_real_escape_string($params['DB_NAME']),
					mysql_real_escape_string($params['DB_TBL']),
					mysql_real_escape_string($params['DB_NAME']),
					mysql_real_escape_string($params['DB_TBL'])
				);

		$res = mysql_query($query);

		// If we have rows, add to stack
		if(!$res){
			$this->db_error = mysql_error();
		} else{
			while($row = mysql_fetch_array($res)){

				if($row['active'] == 'y') $state = "1";
				else if($row['active'] == 'n') $state = "0";
				else $state = "x";

				$bindings = array();
				if( isset($row['bindings'] ) ){
					$binds = explode('|', $row['bindings']);

					foreach($binds as $bind){
						$bindings['item'][] = array(
							'@attributes' => array(
								'host' => $bind,
							),
						);
					}

				}

				$site = array(
					'@attributes' => array(
						'id' => $row['domain_id'],
						'name' => $row['domain'],
						'state' => $state,
						'path' => $row['document_root'],
					),
					'bindings' => array(

						// May need a foreach here in the future for bindings
						$bindings,

					),
				); // site

				array_push($sites, $site);

			} // while $row
		} // if results exist

		// Deploy to class
		$this->sites = $sites;

	} // get_sites

	/*=====================================
	=            Find CMS Info            =
	=====================================*/

	public function get_cms_info($sites){

		foreach($sites as &$site){
			$cms_type = $version = $wp_version = $type_file = $regex = $type = $is_secure = "";
			$matches = $cms_types = $dirs = array();
			$path = $site['@attributes']['path'].'/web';

			$cms_identifiers = array(
				'drupal' => array(
					'/modules/system/system.module' => "/define\('VERSION', '([\d\.]{1,4})'/",
					'/includes/bootstrap.inc'		=> "/define\('VERSION', '([\d\.]{1,4})'/",
				),
				'joomla' => array(
					'/libraries/joomla/version.php'			=> "/RELEASE = '([\d\.]{1,4})'/",
					'/libraries/cms/version/version.php'	=> "/RELEASE = '([\d\.]{1,4})'/",
					'/includes/version.php'					=> "/RELEASE = '([\d\.]{1,4})'/"
				),
				'magento' => array(
					'/app/Mage.php' => "/public static function getVersionInfo\(\)\s*{\s*(return array\(([a-zA-Z0-9'\"=><,]|\s)*)\);/",
				),
				'wordpress' => array(
					'/wp-includes/version.php' => false,
				)
			);


			$dirs = glob($path.'/*', GLOB_ONLYDIR);
			if(empty($dirs)) $dirs = array();	// ensure that we have an array
			array_unshift($dirs, $path);

			// Detect CMS type
			foreach($dirs as $k=>$dir){
				$main_type = $main_ver = "";
				$plugins = array();

				foreach($cms_identifiers as $type=>$cms){

					foreach($cms as $id_file=>$regex){

						// No-regex clause
						if( !$id_file) $id_file = $regex;

						if( file_exists($dir.$id_file)){

							$version = $this->get_cms_version($regex, $type, $id_file, $dir, $cms_identifiers);

							// Get WP plugins
							if( $type == 'wordpress'){
								$plugins = $this->get_plugin_info($dir);

								if( $this->array_searchRecursive('iThemes Security', $plugins)
									&& $this->array_searchRecursive('Stream', $plugins)
								){
									$is_secure = 'yes';
								} else $is_secure = 'no';
							}


							$cms_types['item'][]= array(
								'@attributes' => array(
									'cms' => $type,
									'version' => $version,
									'path' => $dir,
									'isSecure' => $is_secure,
								),
								'plugins' => array( 'item' => $plugins)
							);

							// Save first searched dir info for site node
							if( $k === 0){
								$main_type = $type;
								$type_file = $id_file;
								$main_ver = $version;
							}

							break 2; // done, check next directory
						}

					} // foreach file/regex
				} // foreach CMSs
			} // foreach dirs

			// Save primary CMS if found

			$site['@attributes']['cms'] = $main_type;
			$site['@attributes']['cmsVersion'] = $main_ver;
			$site['cms'] = $cms_types;

		} // foreach sites

		// Deploy to class
		$this->sites = $sites;

	} // get_cms_info


	/*=================================================
	=            Get Google Analytics Code            =
	=================================================*/
	public function get_ga_code($sites){

		foreach($sites as &$site){

			$ga_code = "";
			$url = 'http://'.$site['@attributes']['name'];

			$resp = shell_exec('curl -L '.$site['@attributes']['name'].' | grep -i "UA-"');

			if( preg_match('/[uUaA]{2}-[0-9]{5,7}-[0-9]{1,3}/', $resp, $match)){
				$ga_code = $match[0];
			}

			$site['@attributes']['gaUserAccount'] = $ga_code;
		} // foreach sites

		// fin
		$this->sites = $sites;

	} // get_ga_code

	/*=======================================
	=            Get CMS version            =
	=======================================*/
	public function get_cms_version($regex, $cms_type, $id_file, $dir, $cms_identifiers){

		$version = false;

		// Try matching given file, parse other files if needed
		if( $regex && file_exists($dir.$id_file) ){

			$contents = file_get_contents($dir.$id_file, null, null, 0, 10000);

			// Try first found file
			if( preg_match($regex, $contents, $matches)){

				$version = $matches[1];
			} else{

				// Loop other files, second attempt
				foreach($cms_identifiers[$cms_type] as $file=>$regex_alt){

					if( file_exists($dir.$file)){

						$contents = file_get_contents($dir.$file, null, null, 0, 10000);
						if( preg_match($regex_alt, $contents, $matches)){
							$version = $matches[1];
							break;
						} // if match
					}

				} // foreach files

			} //if initial match

		} // if regex is set and file exists

		// Final detection conditions for sites without regex
		switch($cms_type){

			case 'drupal':
				break;

			case 'joomla':
				break;

			case 'magento':
				$tmp = implode('.', array_filter(eval($version.');')));
				$version = $tmp;
				break;

			case 'wordpress':
				include($dir.$id_file);
				$version = $wp_version;
				break;

			default:

		}

		return $version;
	}

	/*==========================================
	=            Get WP Plugin Data            =
	==========================================*/
	public function get_plugin_info($dir){

		$plugin_output = array();
		$plugins_dir = $dir.'/wp-content/plugins/';
		$default_headers = array(
            'name' => 'Plugin Name',
            //'PluginURI' => 'Plugin URI',
            'version' => 'Version',
            //'Description' => 'Description',
            //'Author' => 'Author',
            //'AuthorURI' => 'Author URI',
            //'TextDomain' => 'Text Domain',
            //'DomainPath' => 'Domain Path',
            //'Network' => 'Network',
            //'_sitewide' => 'Site Wide Only',
        );

		$plugins = glob($dir.'/wp-content/plugins/*', GLOB_ONLYDIR);

		if( is_array($plugins) && !empty($plugins)){
			foreach($plugins as $plugin){

				// Determine plugin basename/filename
				$file_data = "";
				$pname = basename($plugin);
				$output_headers = $default_headers;

				if(file_exists($plugin.'/'.$pname.'.php')) $pfile = $pname.'/'.$pname.'.php';
				else if(file_exists($plugin.'/index.php')) $pfile = $pname.'/index.php';
				else $pfile = glob($plugin.'/*.php');

				// Get headers
				if( !is_array($pfile)){

					// We have identified the local plugin file, parse and extract
					$fp = fopen( $plugins_dir.$pfile, 'r' );
					$file_data = fread( $fp, 8192 );
					fclose( $fp );

				} else {
					// Need to check through files
					foreach($pfile as $file){
						$fp = fopen( $file, 'r' );
						$file_data = fread( $fp, 8192 );
						fclose( $fp );

						if( strpos($file_data, 'Plugin Version') === false) continue;
						else break;
					}
				}

				$file_data = str_replace( "\r", "\n", $file_data );

				//$file_data = file_get_contents($plugins_dir.$pfile, null, null, null, 8192);


				foreach ( $output_headers as $field => $regex ) {
	                if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] ){
	                        $output_headers[ $field ] = trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $match[1] ));
	                } else{
	                        $output_headers[ $field ] = '';
	                }
		        }

		        $output_headers = array_filter($output_headers);
		        if(!empty($output_headers)) $plugin_output[]['@attributes'] = $output_headers;

			} // foreach plugins
		} // if $plugins

		return $plugin_output;
	}

	/*============================================
	=            Calculate Disk Usage            =
	============================================*/

	public function get_disk_use($sites){

		foreach($sites as &$site){

			$dir = rtrim($site['@attributes']['path'], '/').'/';
			if ($dir != "/"){
				$dsize = exec("du -s ".$dir." | awk {'print $1'}", $output);

				// Future formatting?
				$dsize = ($dsize/1000);

				$site['@attributes']['size'] = $dsize;
			}
		}

		// Deploy to class
		$this->sites = $sites;

	} // get_disk_use

	/*==================================
	=            Format XML            =
	==================================*/
	public function format_xml($sites, $stats){

		// Prep XML wrappers
		$xml_array = array(
			'info' => array(
				'@attributes' => $stats
			),	// info
			'server' => array(
				'site' => $this->sites
			) // server
		);

		// Instantiate XML builder
		$temp_xml = Array2XML::createXML('servers', $xml_array);
		$this->xml = $temp_xml->saveXML();

		// Output to file if needed
		if(isset($_REQUEST['output']) && isset($_REQUEST['fmt']) && ($_REQUEST['fmt'] == 'xml') ){
			file_put_contents($_REQUEST['output'].'.xml', $this->xml);
		}

	} // format_xml


	/*===================================
	=            Format JSON            =
	===================================*/
	public function format_json($sites){

		$this->json = json_encode($sites);

		// Output to file if needed
		if(isset($_REQUEST['output']) && isset($_REQUEST['fmt']) && ($_REQUEST['fmt'] == 'json') ){
			file_put_contents($_REQUEST['output'].'.json', $this->json);
		}

	} // format JSON


	public function array_searchRecursive( $needle, $haystack, $strict=false, $path=array() ){
	    if( !is_array($haystack) ) {
	        return false;
	    }

	    foreach( $haystack as $key => $val ) {
	        if( is_array($val) && $subPath = $this->array_searchRecursive($needle, $val, $strict, $path) ) {
	            $path = array_merge($path, array($key), $subPath);
	            return $path;
	        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
	            $path[] = $key;
	            return $path;
	        }
	    }
	    return false;
	}

} // class CSReporter

} // if class !exists









/**
 * Array2XML: A class to convert array in PHP to XML
 * It also takes into account attributes names unlike SimpleXML in PHP
 * It returns the XML in form of DOMDocument class for further manipulation.
 * It throws exception if the tag name or attribute name has illegal chars.
 *
 * Author : Lalit Patel
 * Website: http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes
 * License: Apache License 2.0
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Usage:
 *       $xml = Array2XML::createXML('root_node_name', $php_array);
 *       echo $xml->saveXML();
 */

class Array2XML {

    private static $xml = null;
	private static $encoding = 'UTF-8';

    /**
     * Initialize the root XML node [optional]
     * @param $version
     * @param $encoding
     * @param $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {
        self::$xml = new DomDocument($version, $encoding);
        self::$xml->formatOutput = $format_output;
		self::$encoding = $encoding;
    }

    /**
     * Convert an Array to XML
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DomDocument
     */
    public static function &createXML($node_name, $arr=array()) {
        $xml = self::getXMLRoot();
        $xml->appendChild(self::convert($node_name, $arr));

        self::$xml = null;    // clear the xml node in the class for 2nd time use.
        return $xml;
    }

    /**
     * Convert an Array to XML
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - aray to be converterd
     * @return DOMNode
     */
    private static function &convert($node_name, $arr=array()) {

        //print_arr($node_name);
        $xml = self::getXMLRoot();
        $node = $xml->createElement($node_name);

        if(is_array($arr)){
            // get the attributes first.;
            if(isset($arr['@attributes'])) {
                foreach($arr['@attributes'] as $key => $value) {
                    if(!self::isValidTagName($key)) {
                        throw new Exception('[Array2XML] Illegal character in attribute name. attribute: '.$key.' in node: '.$node_name);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['@attributes']); //remove the key from the array once done.
            }

            // check if it has a value stored in @value, if yes store the value and return
            // else check if its directly stored as string
            if(isset($arr['@value'])) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
                unset($arr['@value']);    //remove the key from the array once done.
                //return from recursion, as a note with value cannot have child nodes.
                return $node;
            } else if(isset($arr['@cdata'])) {
                $node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));
                unset($arr['@cdata']);    //remove the key from the array once done.
                //return from recursion, as a note with cdata cannot have child nodes.
                return $node;
            }
        }

        //create subnodes using recursion
        if(is_array($arr)){
            // recurse to get the node for that key
            foreach($arr as $key=>$value){
                if(!self::isValidTagName($key)) {
                    throw new Exception('[Array2XML] Illegal character in tag name. tag: '.$key.' in node: '.$node_name);
                }
                if(is_array($value) && is_numeric(key($value))) {
                    // MORE THAN ONE NODE OF ITS KIND;
                    // if the new array is numeric index, means it is array of nodes of the same kind
                    // it should follow the parent key name
                    foreach($value as $k=>$v){
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                    // ONLY ONE NODE OF ITS KIND
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]); //remove the key from the array once done.
            }
        }

        // after we are done with all the keys in the array (if it is one)
        // we check if it has any text value, if yes, append it.
        if(!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */
    private static function getXMLRoot(){
        if(empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

    /*
     * Get string representation of boolean value
     */
    private static function bool2str($v){
        //convert boolean to text value.
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }

    /*
     * Check if the tag name or attribute name contains illegal characters
     * Ref: http://www.w3.org/TR/xml/#sec-common-syn
     */
    private static function isValidTagName($tag){
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }
}









/*========================================================
=            Standalone Operation - Edit Here            =
========================================================*/

// Standalone? Print info
if( (ltrim($_SERVER['SCRIPT_NAME'], '/') == basename(__FILE__) ) && class_exists('CSReporter') ){

	$reporter = new CSReporter();

	if( $reporter->db_error) echo "Error: ".$reporter->db_error;

	$_REQUEST['fmt'] = isset($_REQUEST['fmt']) ? $_REQUEST['fmt'] : "";

	switch($_REQUEST['fmt']){
		case 'xml':
			header("Content-type: text/xml; charset=utf-8");
			echo $reporter->xml;
			break;

		case 'json':
			header("Content-type: application/json; charset=utf-8");
			echo $reporter->json;
			break;

		default:
			echo '<pre>'.print_r($reporter, true).'</pre>';
	}


} // if standalone
