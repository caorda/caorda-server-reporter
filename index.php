<?php

include('reporter.php');

$reporter = new CSReporter( array(
    'DB_NAME' => 'dbispconfig',
    'DB_USER' => 'caorda.qa',
    'DB_PASS' => 'Sidney.11022',
    'DB_HOST' => 'mysqlqa.caorda.local',
    'DB_TBL' => 'web_domain',
  ));

?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Caorda Server Reporter</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>

    <style type="text/css">
      #intro .pad-top{ padding-top:20px; text-align: right;}
    </style>
  </head>
  <body>

    <div class="row" id="intro">
      <div class="large-9 columns">
        <h1>Caorda Server Reporter</h1>
      </div>
      <div class="large-3 columns pad-top">
        <a href="/reporter.php?fmt=xml" class="button tiny radius secondary">XML</a>
        <a href="/reporter.php?fmt=json" class="button tiny radius success">JSON</a>
        <a href="/reporter.php" class="button tiny radius alert">PHP</a>
      </div>
    </div>

    <div class="row" id="connection">
      <div class="large-12 columns">

      <div data-alert class="alert-box">
        <?php if( $reporter->db_error): ?>
          failed connection to <?php echo $reporter->db_cred['DB_HOST']; ?> : <?php echo $reporter->db_error; ?>
        <?php else: ?>
          Connected to server: <?php echo $reporter->db_cred['DB_HOST']; ?>
        <?php endif; ?>
        <a href="#" class="close">&times;</a>
      </div>

      </div>
    </div>

    <div class="row" id="sites">
      <div class="large-12 columns">
        <table >
          <thead>
            <tr>
              <th width="40">ID</th>
              <th width="250">Name</th>
              <th width="60">Active</th>
              <th>Path</th>
              <th width="100">CMS</th>
              <th width="70">Version</th>
              <th width="60">Size (MB)</th>
              <th>Bindings</th>
              <th>CMSs</th>
              <th>GA Code</th>
              <th>Secure?</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($reporter->sites as $site):
              $atts = $site['@attributes'];
              $binds = $site['bindings'][0];
              $chcms = $site['cms'];
              //echo '<pre>'.print_r($chcms, true).'</pre>';
            ?>
              <tr>
                <td><?php echo $atts['id']; ?></td>
                <td><?php echo $atts['name']; ?></td>
                <td><?php echo ($atts['state'] == 1) ? "Yes" : "No"; ?></td>
                <td><input type="text" value="<?php echo $atts['path']; ?>" /></td>
                <td><?php echo ucfirst($atts['cms']); ?></td>
                <td><?php echo $atts['cmsVersion']; ?></td>
                <td><?php echo isset($atts['size']) ? $atts['size'] : ""; ?></td>
                <td><textarea><?php echo isset($binds['item'][0]['@attributes']['host']) ? $binds['item'][0]['@attributes']['host'] : ""; ?></textarea></td>
                <td><?php echo isset($atts['gaUserAccount']) ? $atts['gaUserAccount'] : ""; ?></td>
                <td>
                  <?php if(isset($chcms['item'])): foreach($chcms['item'] as $cms): ?>
                    <small title="<?php echo $cms['@attributes']['path']; ?>">
                      <?php echo $cms['@attributes']['cms'].' '.$cms['@attributes']['version']; ?>
                    </small><br />
                  <?php endforeach; endif; ?>
                </td>
                <td><?php echo isset($chcms['item']) ? $chcms['item'][0]['@attributes']['isSecure'] : ""; ?></td>
              </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
      </div>
    </div>

    <div class="row" id="stats">
      <div class="large-12 columns">
        <div class="panel">
          Stats
            <ul>
              <li>Memory usage: <?php echo round(memory_get_usage(true)/1048576,2); ?> MB</li>
              <li>Run time: <?php echo $reporter->stats['time_total']; ?> seconds</li>
              <?php foreach($reporter->stats as $name=>$time): ?>
                <li><small>Timer <?php echo $name; ?>: <?php echo $time; ?></small></li>
              <?php endforeach; ?>
            </ul>
        </div>
      </div>
    </div>

    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
